# Conditions Générales d’Utilisation et de Vente de PAIS

Les présentes conditions générales d’utilisation et de vente (ci-après dénommées « Conditions Générales ») sont conclues entre : 
- La société PAIS société à responsabilité limitée, dont le siège social est situé 113, chemin Hermitte, 83500 La Seyne sur Mer (ci-après : la « Société »).
- L’Utilisateur de l’application PAIS.

## Préambule 

La Société exploite une Application disponible sur IOS et Android, proposant une place de marché en ligne dédiée à la mise en relation entre particuliers qui désirent pratiquer un sport.
Les présentes Conditions Générales ont pour objet de régir les conditions et modalités d’utilisation et de vente du Site et de l’Application, ainsi que de définir les droits et obligations des Utilisateurs.
Tout accès et/ou utilisation de l’Application suppose l’acceptation sans réserves et le respect de l’ensemble des termes des présentes Conditions Générales. PAIS se réserve le droit, à tout moment et unilatéralement, de mettre fin aux présentes CGU, de les compléter ou de les modifier de la façon qu’elle juge nécessaire. Chaque nouvelle version des CGU reçoit une application immédiate pour l’ensemble des Membres dès sa publication sur le site et/ou l’application PAIS.

## 1 - Définitions
**Utilisateur :** Toute personne bénéficiant d’un espace personnel sur le Site (le « Mon Profil »).
**Application :** désigne l’application mobile PAIS disponible en téléchargement gratuit sur https://itunes.apple.com et https://play.google.com</p>
**Mobile Connect :** Fonctionnalité qui permet aux Membres d'utiliser leur numéro de téléphone pour s'identifier sur l'Application.
**Évènement :** Evènement sportif organisé par un utilisateur ou par PAIS, auquel tous les utilisateurs peuvent accéder.
**Application :** désigne l’application mobile PAIS disponible en téléchargement gratuit sur https://itunes.apple.com et https://play.google.com
** Abonnement Premium : ** Souscription au service payant de PAIS donnant accès à plusieurs avantages pour l’utilisateur.
** RGPD ** : Réglement Européen n°2016/679 dit réglèment général sur la protection des données.

## 2 - Inscription au Site
Pour pouvoir accéder aux Services, l’Utilisateur doit avoir plus de 13 ans. L’Utilisateur doit être capable juridiquement de contracter et d’utiliser le Site conformément aux présentes Conditions Générales. L’Utilisateur est tenu de fournir des informations exactes qu’il s’engage à mettre immédiatement à jour en cas de modifications.
L’accès au compte créé un numéro de téléphone ainsi qu'une confirmation SMS.
L’inscription s’effectue via Mobile Connect, qui permet à PAIS d'authentifier l'utilisateur grâce à son numéro de téléphone.
Dans l'hypothèse où l’Utilisateur fournirait des données fausses, inexactes, périmées ou incomplètes, la Société sera en droit de suspendre ou de procéder à la fermeture de son compte et de lui refuser, à l'avenir, l'accès à tout ou partie des Services.

## 3 - Description des Services et fonctionnement du Site
L’Application fournit un cadre convivial qui favorise l’échange et l’entraide entre Utilisateurs, afin de leurs faciliter l’organisation d'événements sportifs.
L’enregistrement d’un Client via la création d’un Compte Personnel est la condition préalable à toute réservation et paiement d’une réservation pour participer à une Événement sportif.
Lorsqu'un Utilisateur souhaite s’inscrire à un Événement, il doit tout d'abord prendre connaissance des conditions et des détails de l’évènement sur sa fiche de présentation, avant d’effectuer sa réservation (par exemple les zones géographiques, le nombre de personnes maximum pouvant assister à l'évenement, le prix, etc.).

## 4 – Accès au Site et aux Services
L’accès au Site et à l’Application est exclusivement réservé aux Utilisateurs inscrits.
Les Utilisateurs font leur affaire personnelle de la mise en place des moyens informatiques et de télécommunications permettant l’accès au Site. Ils conservent à leur charge les frais de télécommunication lors de l’accès à internet et de l’utilisation du Site.
Le Site est accessible 24h/24, 7j/7 pour l’ensemble des Utilisateurs.
La Société se réserve le droit, sans préavis, ni indemnité, de fermer temporairement ou définitivement le Site ou l’accès à un ou plusieurs Services pour effectuer une mise à jour, des modifications ou un changement sur les méthodes opérationnelles, les serveurs et les heures d’accessibilité, sans que cette liste ne soit limitative.
La Société se réserve le droit d’apporter au Site et aux Services toutes les modifications et améliorations qu’elle jugera nécessaires ou utiles dans le cadre du bon fonctionnement du Site et de ses Services.

## 5 – Engagements
5.1 L’Utilisateur s’engage à accéder et utiliser le Site et les Services conformément aux lois en vigueur et aux présentes Conditions Générales. A cet égard, l’Utilisateur reconnait qu’aux fins exclusives de vérification du respect par lui des présentes Conditions Générales et des lois applicables, la Société peut prendre connaissance de tout Contenu publié ou échangé sur le Site.
5.2 L’utilisateur s’interdit d’utiliser les Services et le Site pour faire la promotion de son activité ou de celle d’un tiers. A ce titre, il s’engage notamment à ne pas envoyer de message publicitaire aux autres utilisateurs du Site ou de l’Application.
5.3 Chaque utilisateur qui participe à un Événement s'engage à souscrire toutes les polices d'assurances couvrant sa responsabilité civile.

## 6 – Responsabilité
 
##  Responsabilité des Utilisateurs
 
6.1 L’Utilisateur est seul responsable du préjudice direct ou indirect qu’il est susceptible de subir du fait d’informations inexactes, incomplètes, et/ ou trompeuses qu’il fournirait lors de son inscription ou en l’absence de mise à jour de ces informations, ce dont il assume seul les conséquences.
6.2 L’Utilisateur est seul responsable de l’ensemble des Contenus qu’il choisit de mettre en ligne sur le Site, la Société ne contrôlant pas le Contenu avant la mise en ligne. 
6.3 La Société met tous les moyens en œuvre pour assurer l’accès et le bon fonctionnement du Site et des Services 24 heures sur 24, 7 jours sur 7. Néanmoins, compte tenu des limites liées à l’internet, la Société ne peut exclure que l’accès et le fonctionnement du Site et des Services soient interrompus notamment en cas de force majeure, de mauvais fonctionnement des équipements de l’Utilisateur, de dysfonctionnements du réseau internet de l’Utilisateur, d’opération de maintenance destinées à améliorer le Site et les Services.
En conséquence, la Société ne saurait être tenue responsable d’une interruption des Services, qu’elle soit volontaire ou non, étant précisé qu’elle s’engage à faire ses meilleurs efforts pour limiter les interruptions qui lui seraient imputables.
6.4 La Société met à la disposition des Utilisateurs des outils et moyens techniques leurs permettant d’entrer en relation aux fins d’organiser un Événement et de pratiquer leur sport. Sa responsabilité se limite à la fourniture de ces moyens. La Société et l’Utilisateur sont des parties indépendantes, chacun agissant en son nom personnel et pour leur propre compte.
6.5 La Société ne saurait être tenue pour responsable des informations fausses, trompeuses ou non à jour qui lui sont communiquées par les Utilisateurs.

## 7 – Interactions avec d’autres utilisateurs
Vous êtes seul responsable de vos interactions avec les autres utilisateurs. Vous comprenez que la société ne fait actuellement pas de contrôles pénales sur ses utilisateurs. La société ne vérifie pas les informations communiquées par les utilisateurs. La société ne fait aucune déclaration ou garantie quant à la conduite d'utilisateurs ou à sa compatibilité avec tous utilisateurs actuels ou futurs.
La Société n'est pas responsable de la conduite de tout utilisateur. La Société, ses sociétés affiliées ou ses partenaires ne peuvent en aucun cas être tenus responsables (directement ou indirectement) des pertes ou dommages quelconques, directs, indirects, généraux, spéciaux, compensatoires ou consécutifs, et / ou accessoire, découlant de la conduite de vous ou de quiconque en relation avec l'utilisation du Service.
Vous acceptez de prendre toutes les précautions nécessaires dans toutes les interactions avec d'autres utilisateurs, en particulier si vous décidez de communiquer hors du Service.

## 8 – Données personnelles
Conformément à la loi dite « Informatique et Libertés » du 6 janvier 1978 modifiée par la loi du 6 août 2004, l’Utilisateur est informé que la Société procède à des traitements automatisés des données à caractère personnel de l’Utilisateur, notamment lors de sa connexion ou inscription au Site.
La Société est destinataire des données à caractère personnel recueillies par l'intermédiaire du Site et de l’Application. Elle s’engage à mettre tous les moyens en œuvre pour assurer la sécurité et la confidentialité de ces données.
Ces données sont destinées à permettre à la Société de rendre les Services accessibles à l’Utilisateur et sont également utilisées à des fins de statistiques, de prospection commerciale directe, d’envoi de newsletters et de SMS.
La Société s’engage à ne pas partager ces données, si un contrat de partenariat définissant l’utilisation de ces dernières n’a pas été rédigé et signé en amont avec le destinataire. Les destinataires des données peuvent être les partenaires inscrits sur le Site ou l’Application et mentionnés comme tels.
Conformément aux dispositions de la loi dite « informatique et liberté » du 6 janvier 1978 modifiée par la loi du 6 août 2004, l’Utilisateur dispose d'un droit d'accès, de modification, de rectification et de suppression des données à caractère personnel qui le concernent.
L’Utilisateur peut également s’opposer à ce traitement pour des motifs légitimes.
Pour exercer ses droits, il suffit à l’Utilisateur d’écrire à l’adresse électronique suivante « contact@pais-sports.fr ».

## 9 – RGPD
Dans une volonté d'adaptation au Réglement Européen n°2016/679, dit réglèment général sur la protection des données, la société PAIS a mis en oeuvre une série d'actions détaillées ci-dessous : 
- La nomination d'un délégué à la protection des données personnelles en la personne de Bastien Bagliotto, reponsable de la mise en conformité dans le cadre de la RGPD, de la sensibilisation et la diffusion d'une culture "informatique et libertés".
- L'élaboration d'un registre de traitement des données.
- La mise en place d'une stratégie de sécurisation des données.
- La mise en place d'explication claires et transparentes dans l'application, avant chaque demande d'informations personnelles relatives à l'utilisateur et à son utilisation des fonctionnalités de l'application.
- La possibilité pour l'utilisateur de consulter, de modifier et de supprimer les données récoltées à son sujet en adressant un courrier électronique à l’adresse suivante : "contact@pais-sports.fr". 
    
## 10 – Durée, résiliation et sanctions
Le présent contrat est conclu pour une durée indéterminée à compter de l’acceptation des Conditions Générales par l’Utilisateur.
Les Contrats demeureront en vigueur jusqu’à leur résiliation, à votre initiative, ou à celle de PAIS. Vous convenez et reconnaissez cependant que la licence que vous avez concédée, en relation avec le Contenu utilisateur, comprenant les Commentaires, est irrévocable, et continuera donc après l’expiration ou la résiliation de l’un des Contrats pour quelque raison que ce soit. Vous pouvez mettre fin aux Contrats à tout moment sans raison. Dans une telle hypothèse vous ne serez pas remboursés pour le mois en cours. PAIS peut à tout moment résilier les Contrats ou suspendre votre accès au service PAIS Premium sans information préalable et sans préavis en cas de faute grave de votre part, et notamment en cas de violation par vous des lois applicables, de violation par vous de l’article 8 ci-avant ou de non-respect des droits des tiers. 
Les sections 6, 7, 8, 9, 11 des présentes, ainsi que toute autre section des Contrats, qui, explicitement, ou par leur nature, doivent demeurer en vigueur après la résiliation des Contrats, survivront à la résiliation.

## 11 – Service clientèle
Pour toute question ou information concernant le Site et les Services, l’Utilisateur peut contacter la Société en lui adressant un courrier électronique à l’adresse suivante : « contact@pais-sports.fr »

## 12 – Nullité – Renonciation
Dans l’hypothèse où l’une des clauses du présent contrat serait déclarée nulle et non avenue par un changement de législation, de réglementation ou par une décision de justice, cela ne saurait en aucun cas affecter la validité et le respect des présentes Conditions Générales.
Le défaut pour la Société d’exercer les droits qui lui sont reconnus en application des présentes ne constitue pas une renonciation à faire valoir ses droits.
    
## 13 – Droit applicable et juridiction compétente
Les présentes Conditions Générales sont soumises au droit français. Tout litige relatif à leur formation, conclusion, interprétation et/ou exécution relève de la compétence exclusive des juridictions dans le ressort de la Cour d’appel de Paris.

Date d’entrée en vigueur : le 11/05/2021
